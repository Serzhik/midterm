package midterm.repository;

import midterm.entity.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {


    @Query(value = "select * from person where id = 2", nativeQuery = true)
    Person getUsers();

    List<Person> findAll();
}