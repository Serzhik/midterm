package midterm.controller;

import midterm.entity.Person;
import midterm.repository.PersonRepository;
import midterm.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users/")
    public ResponseEntity<?> getPerson(@PathVariable Long personId) {
        return ResponseEntity.ok(personService.getById(personId));
    }

    @GetMapping("/api/v2/users/")
    public ResponseEntity<?> getPerson() {
        return ResponseEntity.ok(personService.getAll());
    }

    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> savePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @PutMapping("/api/users")
    public ResponseEntity<?> updatePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deletePerson(@PathVariable Long personId) {
        personService.delete(personId);
    }
}
