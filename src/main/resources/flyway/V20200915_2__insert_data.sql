
insert into person (id, firstname, lastname, city, phone, telegram)
values(1, 'Lorri', 'Luberti', 'São Caetano do Sul', '8032387590', false),
 (2, 'Forster', 'Marquez', 'Xinzhu', '1866875791', true),
 (3, 'Gonzalo', 'Necolds', 'Linshi', '8216644829', true),
 (4, 'Viviyan', 'Yacobsohn', 'Jiudu', '9718731177', false),
 (5, 'Lorry', 'Dobbing', 'Araal', '8879289785', true),
 (6, 'Alaine', 'Purkiss', 'Ocoruro', '6068858577', false),
 (7, 'Les', 'Chevers', 'Rudy', '8234337635', false),
 (8, 'Janetta', 'Matijasevic', 'Zerkten', '5052349892', false),
 (9, 'Kristian', 'Foucher', 'Tomari', '7964974824', false),
 (10, 'Allister', 'Martel', 'Qarqania', '6746339100', false);