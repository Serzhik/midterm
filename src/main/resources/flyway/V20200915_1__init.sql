
DROP TABLE IF EXISTS person;
create table person (
                      id serial not null
                          constraint person_pk
                              unique,
                      firstName varchar default 255,
                      lastName varchar default 255 ,
                      city varchar default 255 ,
                      phone bigint,
                      telegram bool
);

alter table person owner to postgres;

